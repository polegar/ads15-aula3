
Atividade 2 - ADS15
---                                                                                                                                                

**Módulo 3** - Gerência de containers com Docker e Kubernetes 

Resolução do exercício via terminal

## Acompanhar Resolução                                                                                                                            

Para acompanhar a resolução do exercício, execute no terminal o seguinte comando:

```bash                                                                                                                                            
scriptreplay --timing=comandos.log comandos.txt                                                                                                    
```                                                                                                                                                

Acompanhe a resolução que será apresentada no terminal.

